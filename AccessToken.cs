﻿using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class AccessToken
{
    public string AzureADAccessToken { get; set; }
    private IConfidentialClientApplication App { get; set; }
    private string[] ResourceIds { get; set; }
    private Microsoft.Identity.Client.AuthenticationResult Result { get; set; }
    private string Instance { get; set; }
    private string TenantID { get; set; }
    private string ClientID { get; set; }
    private string ClientSecret { get; set; }
    private string ResourceID { get; set; }
    private string Authority { get; set; }

    public AccessToken()
    {
        this.Instance = ConfigurationManager.AppSettings["Instance"];
        this.TenantID = ConfigurationManager.AppSettings["TenantID"];
        this.ClientID = ConfigurationManager.AppSettings["ClientID"];
        this.ClientSecret = ConfigurationManager.AppSettings["ClientSecret"];
        this.ResourceID = ConfigurationManager.AppSettings["ResourceID"];
        this.Authority = String.Format(CultureInfo.InvariantCulture, this.Instance, this.TenantID);
    }
    public async Task<string> GetToken()
    {
        this.App = ConfidentialClientApplicationBuilder.Create(this.ClientID)
            .WithClientSecret(this.ClientSecret)
            .WithAuthority(new Uri(this.Authority))
            .Build();

        this.ResourceIds = new string[] { this.ResourceID };
        this.Result = await App.AcquireTokenForClient(this.ResourceIds).ExecuteAsync();
        this.AzureADAccessToken = this.Result.AccessToken;
        return this.AzureADAccessToken;
    }
}
