﻿using Microsoft.Identity.Client;
using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAccessToken
{
    class Program
    {
        static AccessToken gat = new AccessToken();
        static string AzureADAccessToken { get; set; }

        static async Task Main(string[] args)
        {
            await GetToken();
        }

        static async Task<string> GetToken()
        {
            await gat.GetToken();
            AzureADAccessToken = gat.AzureADAccessToken;
            Console.WriteLine($"{AzureADAccessToken}");
            File.WriteAllText("jwt.txt",AzureADAccessToken);
            return AzureADAccessToken; 
       }
    }
}
